import { configureStore } from "@reduxjs/toolkit";
import articleSlice from "./articleSlice";
import authSlice from "./authSlice";




export const store = configureStore({
    reducer: {
        auth: authSlice,
        article: articleSlice
    }
});
