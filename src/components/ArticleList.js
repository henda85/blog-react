import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchArticles } from "../stores/articleSlice";
import { Row, Col, Divider } from 'antd';
 import { Article } from "./Article"; 



export function ArticleList(){
    const dispatch = useDispatch();
    const articles = useSelector(state=>state.article.list);

    useEffect(()=>{
    dispatch(fetchArticles());
    },[dispatch]);
    

    return(
        <Row  gutter={[16,16]}>
            <Col span={24}><Divider><h1>BLOG</h1></Divider></Col>
            {articles.map(item=>
                <div key={item.id}>
                    <Article article={item}/>
                </div>)}
        </Row>
    )
}
