import { Menu } from "antd";
import { useDispatch, useSelector } from "react-redux";
import  "./Nav.css";
import { Link, useLocation } from "react-router-dom";
import { logout } from "../stores/authSlice";


export function Nav() {

    const location = useLocation();
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);

    return (
        <Menu mode="horizontal" theme="light" selectedKeys={[location.pathname]}>
            <Menu.Item  className="modified-item" key="/">
                <Link to="/">Home
                </Link>
            </Menu.Item>
            {user ? <>
            <Menu.Item  className="modified-item" key="/addPost">
                    <Link to="/addPost">AddPost</Link>
                </Menu.Item>
            <Menu.Item  className="modified-item" key="/logout">
                    <Link to="/logout"onClick={() => dispatch(logout())}>Logout</Link>
                </Menu.Item>
                </>
            :
                <>
                <Menu.Item key="/register">
                    <Link to="/register">Register</Link>
                </Menu.Item>

                <Menu.Item key="/login">
                    <Link to="/login">Login</Link>
                </Menu.Item>
            </>}
        </Menu>
    )
}
