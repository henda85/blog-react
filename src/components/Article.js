import { Card, Col } from 'antd';

export function Article({article}){
    const { Meta } = Card;
    return(
        <Col span={8}>
            <Card
        hoverable
        style={{ width: 240 }}
        cover={<img alt="example" src={article.contenue} 
        />}
    >
        <Meta title={article.titre} description={article.userIdFk} />
    </Card>
    </Col>
    );
}