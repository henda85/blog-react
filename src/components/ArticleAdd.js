
    import { Form, Input, Button } from 'antd';
    import { useDispatch} from 'react-redux';
    import { useHistory } from 'react-router-dom';
    import { addPost } from '../stores/articleSlice';


    export function ArticleAdd(){

    const dispatch= useDispatch();
    let history= useHistory();

    function handleClick(){
    history.push("/");
    }

    const onFinish = (values)=>{
    dispatch(addPost(values));
    handleClick();
    }
    return ( 
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            >
            <Form.Item
                label="Titre"
                name="titre"
                rules={[
                    {
                    required: true,
                    message: 'Titre de l article !',
                    }
                ]}
            >
            <Input/>
                </Form.Item>
                    <Form.Item
                        label="Image"
                        name="contenue"
                            rules={[
                            {
                                required: true,
                                message: 'Entrer le lien de ton image!',
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Date"
                        name="date"
                        rules={[
                            {
                            required: true,
                            message: 'choisissez une date',
                            },
                        ]}
                        >
                        <Input type="date"/>
                    </Form.Item>
                        
                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                </Form.Item>
        </Form>
    );
};
                
