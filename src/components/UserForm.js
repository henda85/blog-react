    import { Form, Input, Button } from 'antd';
    import { useDispatch, useSelector } from 'react-redux';
    import { useHistory } from 'react-router-dom';
    import {  register } from '../stores/authSlice';

    export function UserForm(){
    const dispatch= useDispatch();
    const feedback=useSelector(state=>state.auth.registerFeedback);

    let history= useHistory();

    function handleClick(){
    history.push("/");
    }

    const onFinish = (values)=>{
        dispatch(register(values));
    handleClick()
    }
    return (
        <Form
        name="basic"
        labelCol={{
            span: 8,
        }}
        wrapperCol={{
            span: 16,
        }}
        initialValues={{
            remember: true,
        }}
        onFinish={onFinish}
        >
            {feedback && <p>{feedback}</p>}
            <Form.Item
            label="nom"
            name="nom"
            rules={[
            {
                required: false,
                message: 'Please input your name!',
            },
            ]}
        >
            <Input/>
        </Form.Item>
        <Form.Item
            label="Email"
            name="email"
            rules={[
            {
                required: true,
                message: 'Please input your email!',
            },
            {
                type: 'email',
                message: 'Please enter a valid email',
            },
            ]}
        >
            <Input type="email" />
        </Form.Item>

        <Form.Item
            label="Password"
            name="password"
            rules={[
            {
                required: true,
                message: 'Please input your password!',
            },
            ]}
        >
            <Input.Password />
        </Form.Item>
        
            <Form.Item
            wrapperCol={{
            offset: 8,
            span: 16,
            }}
        >
            <Button type="primary" htmlType="submit">
            Submit
            </Button>
        </Form.Item>
        </Form>
    );
    };


