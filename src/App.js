    import { useEffect } from "react";
    import { useDispatch } from "react-redux";
    import { Route, Switch } from "react-router-dom";
    import './App.css';
    import { Nav } from "./components/Nav";
    import { AddPost } from "./pages/AddPost";
    import { Home } from './pages/Home';
    import { Login } from "./pages/Login";
    import { Register } from "./pages/Register";
    import { loginWithToken } from "./stores/authSlice";

    export function App() {
    const dispatch= useDispatch();
    useEffect(() => {
        dispatch(loginWithToken());
    }, [dispatch]);
    return (
        <>
        <header>
            <Nav/>
        </header>
            <Switch>
            <Route path="/register">
                <Register />
            </Route >
            <Route path="/login">
                <Login />
            </Route >
            <Route path="/" exact>
                <Home />
            </Route >
            <Route path="/addPost">
                <AddPost/>
            </Route>
            </Switch>
        </>
    );
    }

    export default App;
