import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";



 const articleSlice = createSlice({
    name: 'article',
    initialState:{
        list:[]
    },
    reducers:{
        setList(state,{payload}){
            state.list = payload;
        },
        addArticle(state,{payload}){
            state.list.push(payload);
        }
    }
});

export const { setList, addArticle} = articleSlice.actions;
export default articleSlice.reducer;

export const fetchArticles = () => async (dispatch)=>{
    try {
        const response = await axios.get(process.env.REACT_APP_SERVER+'/api/article');
        dispatch(setList(response.data));
        
    }catch (error){
        console.log(error);
    }
}

export const addPost = (body)=> async(dispatch)=>{
    try {
        const response = await axios.post(process.env.REACT_APP_SERVER+'/api/article/',body);
        dispatch(addArticle(response.data));
        dispatch(fetchArticles());
    } catch (error) {
        console.log(error);
    }
}