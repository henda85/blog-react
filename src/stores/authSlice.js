
import axios from "axios";
import { createSlice } from '@reduxjs/toolkit';


 const authSlice = createSlice({
        name: 'auth',
        initialState:{
            user : null,
            loginFeedback: '',
            registerFeedback: ''
            
        },
        reducers: {    
            login(state,{payload}){
                state.user=payload;
            },
            logout(state){
                state.user=null;
                localStorage.removeItem('token');
            },
            updateRegisterFeedback(state,{payload}){
                state.registerFeedback=payload;
            },
            updateLoginFeedback(state,{payload}){
                state.loginFeedback=payload;
            }
            
        }
})

 export const {login, logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;

 export default authSlice.reducer;

export const loginWithToken=()=> async (dispatch)=>{
    const token = localStorage.getItem('token');
    if(token) {
        try {
            const response = await axios.get(process.env.REACT_APP_SERVER+'/api/user/account');
            dispatch(login(response.data));
        } catch(error) {
            dispatch(logout());
        }
    }
    
}
export const register = (body) => async (dispatch) => {

    try {
        const {user, token} = await axios.post(process.env.REACT_APP_SERVER+'/api/user', body);
        localStorage.setItem('token', token);
        dispatch(updateRegisterFeedback('Registration successful'));
       // dispatch(addUser(user));
        dispatch(login(user));

    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
    }
}

export const loginWithCredentials = (credentials) => async (dispatch) => {

    try {
        const response = await axios.post(process.env.REACT_APP_SERVER+'/api/user/login', credentials);
        
        localStorage.setItem('token', response.data.token);
        dispatch(updateLoginFeedback('Login successful'));
        dispatch(login(response.data.user));
        console.log('login');
    } catch (error) {
        dispatch(updateLoginFeedback('Email and/or password incorrect'));
    }
}





